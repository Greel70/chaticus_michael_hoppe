package edu.lsus.chaticus

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_new_message.*

class NewMessageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            //Todo: POST to API

            Snackbar.make(view, "Message sent", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            val intent = Intent(this, ConversationActivity::class.java)
            startActivity(intent)
        }
    }

}
