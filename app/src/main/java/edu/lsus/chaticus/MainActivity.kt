package edu.lsus.chaticus

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlinx.android.synthetic.main.list_item_view.view.*
import edu.lsus.chaticus.R.id.recyclerView
import android.support.v7.widget.DividerItemDecoration
import edu.lsus.chaticus.R.id.recyclerView


class MainActivity : AppCompatActivity() {
    lateinit var adapter: Adapter
    var list = mutableListOf<String>("","","","","","","","","","","","")
    //<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="User">
// User</a> from <a href="https://www.flaticon.com/"
// title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
// title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = Adapter(list)
        recyclerView.adapter = adapter
        val fab = floatingActionButton
        fab.setOnClickListener {
            val intent = Intent(this, NewMessageActivity::class.java)
            startActivity(intent)
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        //val mLayoutManager = recyclerView.layoutManager
        // val mDividerItemDecoration = DividerItemDecoration(recyclerView.context, mLayoutManager.)
        //recyclerView.addItemDecoration(mDividerItemDecoration)
        if (!app.session.hasValidCredentials()) {
            app.login(this)
        } else {
            app.api.getMessages(1).enqueue(object : Callback<List<MessageDto>> {
                override fun onResponse(call: Call<List<MessageDto>>?, response: Response<List<MessageDto>>?) {

                    //adapter = Adapter(response)
                }

                override fun onFailure(call: Call<List<MessageDto>>?, t: Throwable?) {

                }
            })
        }
    }

    //todo: add more options on floating action button

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_about -> {
                //TODO: About us
                AlertDialog.Builder(this@MainActivity).setTitle("About Us").setMessage(
                        "Author: Michael Hoppe \n" +
                                "Class: CSC490 \nDate: 5/1/2018 \n" +
                                "Version 0.0.4").setPositiveButton("OK",
                        DialogInterface.OnClickListener {dialog, whichButton ->

                        }).create().show()
                return true
            }
            R.id.menu_settings -> {
                //TODO: Settings intent
                showToast("TODO: Settings intent")
                return true
            }
            R.id.menu_logout -> {
                app.logout(this)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showToast(message: String){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

    class Adapter(val items: MutableList<String>): RecyclerView.Adapter<ViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent!!.context)
            val view = inflater.inflate(R.layout.list_item_view,parent,false)
            val vh = ViewHolder(view)
            vh.recipientName = view.recipient_name
            vh.image = view.image
            vh.messagePreview = view.message_preview
            return vh
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            holder!!.recipientName.text = "Test Name"
            holder.itemView.setOnClickListener {
               //TODO: MAKE INTENT TO OPEN MESSAGES OF A CLICKED ITEM
                val intent = Intent(holder.itemView.context, ConversationActivity::class.java)
                Toast.makeText(holder!!.itemView.context, "Open intent to messages",Toast.LENGTH_SHORT).show()
            }
            holder!!.messagePreview.text = "This is a test message"
            holder!!.image.setImageDrawable (holder.itemView.context.getDrawable(R.drawable.ic_user_default))
            //Picasso.with(holder.itemView.context).load("").placeholder(R.drawable.ic_user_default).into(holder.image)
        }






    }

    class ViewHolder(root: View) : RecyclerView.ViewHolder(root) {
        lateinit var recipientName: TextView
        lateinit var messagePreview: TextView
        lateinit var image: ImageView
    }
}